﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OrderForm
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        /// <summary>
        /// To display ordered drink and sugar
        /// </summary>
        public void btnOrder_Click(Object sender,RoutedEventArgs e)
        {
            if (drink.Text == "")
            {
                MessageBox.Show("No drink ordered");
                drink.Focus();
                return;
            }
            int sugarCount;
            try
            {
                sugarCount = Int32.Parse(sugar.Text);
            }
            catch
            {
                MessageBox.Show("You haven't ordered any sugars");
                sugar.Focus();
                return;
            }
            MessageBox.Show("You have order a "+drink.Text+" with "+sugarCount+" sugars ","Order confirmation");
        }
    }
}
